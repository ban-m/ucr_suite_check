#!/bin/bash
READ=/glusterfs/ban-m/E_coli_K12_1D_R9.2_SpotON_2/pass.fq
REFERENCE=./data/reference.fa
ANSWER=/glusterfs/ban-m/ecoli/K121DR92.paf

minimap2 -x map-ont ${REFERENCE} ${READ} > ${ANSWER}
echo -e "read_id\tlength\tstart\tend\tstrand\treference_name\tlength\tstart\tend" > ./data/answer.tsv
cat ${ANSWER} | sed -e 's/_Basecall_Alignment_template//g' | cut -f -9 > ./data/answer.tsv

REFERENCE_SUBSET=./data/ecolik12-0-200000.fa
REFERENCE_SIGNAL=./data/ecolik12-0-200000.csv

samtools faidx ${REFERENCE} NC_000913.3:0-200000 > ${REFERENCE_SUBSET}
cargo run --release --bin convert -- ./data/template_r9.2.csv ${REFERENCE_SUBSET} > ${REFERENCE_SIGNAL}
cat ${REFERENCE_SIGNAL} | tail -n+2 | sed -e 's/,/ /g' > ./data/ucr_reference.txt
