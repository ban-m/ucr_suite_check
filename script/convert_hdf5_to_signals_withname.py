import h5py
import itertools as it
import glob
import numpy as np
import sys


def into_id_and_signal(file):
    '''
    convert file into (ID, signal)
    '''
    with h5py.File(file, 'r') as f:
        path_to_id = [key for key in f['Raw/Reads/'].keys()]
        if len(path_to_id) == 0:
            return ('', [])
        file_id = f['Raw/Reads/' + path_to_id[0]].attrs['read_id']
        events = f['Analyses/Basecall_1D_000/BaseCalled_template/Events']['mean']
        return (file_id, events)


def convert(filenames):
    '''
    convert given file into array of signals with read ID
    '''
    test = 10
    result = [into_id_and_signal(file) for file in it.islice(filenames, test)]
    return (len(result), result)

if __name__ == '__main__':
    FILES = glob.iglob("/glusterfs/ban-m/E_coli_K12_1D_R9.2_SpotON_2/downloads/pass/**/*.fast5", recursive=True)
    (datanum, data) = convert(FILES)
    for (read_id, signal) in data:
        file = './data/signals/' + read_id.decode('utf-8') + '.txt'
        with open(file, 'w') as output:
            if len(signal) != 0:
                print(' '.join(map(str, signal)), file=output)
