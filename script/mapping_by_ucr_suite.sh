#!/bin/bash
QUERY=./data/signals/
REFERENCE=./data/ucr_reference.txt
ANSWER=./result/result_UCR.txt

function map(){
    for file in ${QUERY}/*.txt
    do
        echo $file >> ${ANSWER}
        ./exec/ucr_suite ${REFERENCE} ${file} 250 0.10 >> ${ANSWER}
        echo "" >> ${ANSWER}
    done
}

time map 
