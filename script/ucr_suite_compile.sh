#!/bin/bash
## Get source file and compile.
mkdir -p exec build
cd build 
wget https://www.cs.ucr.edu/~eamonn/trillion.zip
unzip trillion.zip
find ./ -name "*.cpp" | grep DTW | xargs -Icode mv code ucr_suite_dtw.cpp
cd ..
g++ -Wall -O3 -o ./exec/ucr_suite ./build/ucr_suite_dtw.cpp -lm 
