#!/bin/bash
REFERENCE=./data/ecolik12-0-200000.csv
ANSWER=./result/result_exact_sub_DTW.txt
time cargo run --release --bin mapping "./data/signals/*.txt" ${REFERENCE} > ${ANSWER}
