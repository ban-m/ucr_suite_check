extern crate squiggler;
use std::path::Path;
fn main() {
    let args:Vec<_> = std::env::args().collect();
    let squiggler = squiggler::Squiggler::new(&Path::new(&args[1])).unwrap();
    let event:Vec<_> = squiggler.get_signal_from_path(&Path::new(&args[2])).unwrap()
        .into_iter().map(|e|e.2).collect();
    println!("{}",event.len());
    let len = event.len() as f64;
    let (sum,sqsum) : (f64,f64) = event.iter()
        .fold((0.,0.),|(sum,sqsum),&x|{let x = x as f64;(sum+x,sqsum+x*x)});
    let mean = sum/len;
    let stdev = (sqsum/len - mean*mean).sqrt();
    
    println!("{}",{
        let event:Vec<_> = event.iter().map(|&x|format!("{}",(x as f64 - mean)/stdev)).collect();
        event.join(",")
    });
}
