extern crate squiggler;
extern crate glob;
extern crate dtw;
use std::path::Path;
use std::io::prelude::*;
fn main()->std::io::Result<()>{
    let args:Vec<_> = std::env::args().collect();
    let reference = parse_reference(&args[2])?;
    eprintln!("Reference parsed: {} length", reference.len());
    for querypath in glob::glob(&args[1]).unwrap().filter_map(|e|e.ok()){
        let query = parse_query(&querypath)?;
        let query = dtw::normalize(&query[..250],dtw::NormalizeType::Z);
        let name = querypath.file_stem().unwrap();
        let (score,_,start) = dtw::dtw(&query,&reference,dtw::Mode::Sub,&dist).unwrap();
        println!("{}\t{}\t{}",name.to_str().unwrap(),score.sqrt(), start);
    }
    Ok(())
}

fn parse_reference(path:&str)->std::io::Result<Vec<f32>>{
    let mut file = std::fs::File::open(&Path::new(path))?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;
    Ok(input.lines().nth(1).unwrap().split(',').filter_map(|e|e.parse().ok()).collect())
}

fn parse_query(path:&Path)-> std::io::Result<Vec<f32>>{
    let mut file = std::fs::File::open(path)?;
    let mut input = String::new();
    file.read_to_string(&mut input)?;
    Ok(input.lines().nth(0).unwrap().split_whitespace().filter_map(|e|e.parse().ok()).collect())
}

fn dist(x:&f32,y:&f32)->f32{
    (x-y)*(x-y)
}
