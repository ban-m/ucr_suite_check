# UCR suite check

Autho: Bansho Masutani ban-m@g.ecc.u-tokyo.ac.jp


## Requirement

- [mercurial](https://www.mercurial-scm.org)
- [Rust language](https://www.rust-lang.org))
- *NIX OS (I'm developping in Ubuntu18.04/CentOS6).

## Dataset
- Reference:./data/ucr_reference.txt
- Query:.data/signals/*.txt
- Result:./resut/resut_UCR.txt

For comparison, the exact Sub-DTW are also applied. The output file is `./data/resut_exact_sub_DTW.txt`.

## Synopsis
Just type
```
bash ./script/setup.sh
```

To only produce the result of UCR suite, type
```
bash ./script/mapping_by_ucr_suite.sh
```

To carry out exact sub DTW, type
```
bash ./script/mapping_by_exact.sh
```